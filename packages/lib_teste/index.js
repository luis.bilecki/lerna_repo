module.exports = {
    hello: () => 'hello',
    sayHello: (name) => `Hello, ${name}!`,
}